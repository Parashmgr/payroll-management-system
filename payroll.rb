require "./database"
require "./new_employee"
require "./update_employee"
require "./delete_employee"
require "./tax_deduction"
require "./search"

class Payroll
  #initialize the database for payroll
  def initialize
    #conncetion to the mysql payroll database
    @client = Database.new
  end

  def choice
    puts "enter the choice"
    puts "1.All employee list"
    puts "2.Add a new employee"
    puts "3.Update a employee"
    puts "4.Delete a employee"
    puts "5.Search"
    puts "6.Exit"
    x = gets.chomp.to_i
    case x
    when 1
      @client.query_for_list_employee
      again_choice
    when 2
      @client.query_for_new_employee
      again_choice
    when 3
      @client.query_for_update_employee
      again_choice
    when 4
      @client.query_for_delete_employee
      again_choice
    when 5
      @client.search
      again_choice
    when 6
      exit
    else
      "sorry wrong choice"
    end
  end



  def again_choice
    puts "Do you want to continue???"
    x = gets.chomp
    while (x == "y" || x == "Y")
      choice
      break
    end
  end
end

p = Payroll.new
p.choice
