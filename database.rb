require "mysql2"
require "rubygems"
require "pry"

class Database
  def initialize
    begin
      @client = Mysql2::Client.new(host: "localhost", username: "parash", password: "P@ra$h0205", database: "payroll")
      @client.query("create table if not exists employees(id int not null auto_increment primary key,
        first_name varchar(20),
        last_name varchar(20),
        address varchar(20),
        contact_num varchar(20),
        email varchar(20),
        basic_salary int(10),
        total_salary int(10),
        position varchar(20),
        date timestamp)")
    rescue => exception
      puts exception.message
    end
  end

  def query_for_list_employee
    @result = @client.query("select id,first_name,last_name,address,contact_num,email,basic_salary,total_salary,position,date from employees")
    unless @result.size == 0
      @result.each do |value|
        puts "employee_id=#{value["id"]}\nfirst_name=#{value["first_name"]}\nlast_name=#{value["last_name"]}\n contact_num=#{value["contact_num"]}\naddress=#{value["address"]}\nemail=#{value["email"]}\nbasic_salary=#{value["basic_salary"]}\ntotal_salary=#{value["total_salary"]}\nposition=#{value["position"]}\nstart_date=#{value["date"].strftime("%Y-%m-%d")}"
        puts "-------------------------------------------------------------------------------------------------"
      end
    else
      puts "there is no employee"
    end
  end


   #quey for new employee or adding a new employee


  def query_for_new_employee
    obj = Newemployee.new
    obj.add
    puts "---------------------------------------------------------------------------------------------\n"
    begin
      @new_employee = @client.query("
        INSERT INTO employees 
        (first_name, last_name, address, contact_num, email, basic_salary,total_salary, position )
        VALUES ('#{obj.employee_first_name}',
          '#{obj.employee_last_name}','#{obj.employee_address}',
          #{obj.employee_num},'#{obj.employee_email_address}',#{obj.salary},#{obj.employee_salary},
          '#{obj.employee_position}')
        ")
    puts "new employee is sucessfully added"
  
    rescue => exception
      puts exception.message      
    end
    
  end


   #query for updating the employee details

  def query_for_update_employee
    @result = @client.query("select id,first_name,last_name,address,contact_num,email,basic_salary,total_salary,position,date from employees")
    unless @result.size == 0
      @result.each do |value|
        puts "id=#{value["id"]}\nfirst_name#{value["first_name"]}\nlast_name=#{value["last_name"]}\naddress=#{value["address"]}\ncontact=#{value["contact_num"]}\nemail=#{value["email"]}\nbasic_salary=#{value["basic_salary"]}\ntotal_salary=#{value["total_salary"]}\nposition=#{value["position"]}"
        puts "----------------------------------------------------------------------"
      end
       update_item = Employeeupdate.new
       update_item.update_employee
       to_update_id = update_item.id_to_update

    begin
      @ids=[]
        @results = @client.query("SELECT id from employees").to_a;
        @ids = @results.map { |result| result["id"].to_s }
        if @ids.include? to_update_id.to_s
        type, val, update = update_item.choice_for_update
        end
        case type
          when "contact_num"
            @update_employee = @client.query("update employees set contact_num = #{val} where id = #{to_update_id}" )
          when "address"
            @update_employee = @client.query("update employees set address = '#{val}' where id = #{to_update_id}" )
          when "email"
            @update_employee = @client.query("update employees set email = '#{val}' where id = #{to_update_id}" )
          when "salary"
            @update_employee = @client.query("update employees set basic_salary = #{update} where id = #{to_update_id}" )
            @update_employee = @client.query("update employees set total_salary = #{val} where id = #{to_update_id}" )
          when "position"
            @update_employee = @client.query("update employees set position = '#{val}' where id = #{to_update_id}" )
        end
        if @ids.include? to_update_id.to_s
        puts "sucessfully updated"
        else
          puts "id not found"
        end
  
    rescue => exception
      puts exception.message
    end
  else
    puts "there is no employee"
  end
    
   
  end

  #Deleting the employee


  def query_for_delete_employee
    @result = @client.query("select id,first_name,last_name,address,contact_num,email,basic_salary,total_salary,position from employees")
    unless @result.size == 0
      @result.each do |value|
        puts "id=#{value["id"]}\nfirst_name#{value["first_name"]}\nlast_name=#{value["last_name"]}\naddress=#{value["address"]}\ncontact=#{value["contact_num"]}\nemail=#{value["email"]}\nbasic_salary=#{value["basic_salary"]}\ntotal_salary=#{value["total_salary"]}\nposition=#{value["position"]}"
        puts "----------------------------------------------------------------------"
      end
    delete_item = Deleteemployee.new
    delete_item.delete_employee
    to_delete_id = delete_item.del_employee
    begin
      @ids=[]
        @results = @client.query("SELECT id from employees").to_a;
        @ids = @results.map { |result| result["id"].to_s }
        @remove_employee = @client.query("delete from employees where id = '#{delete_item.del_employee}'")
        if @ids.include? to_delete_id.to_s
        puts "sucessfully deleted"
        else
          puts "id not found"
        end
  
    rescue => exception
      puts exception.message
    end
  else
    puts "there is no employee"
  end
    #end of do loop
  
  end#end for the method of delete
  def search
        results = "" 
        @sc_database = Search.search_first_name
        @search_result = @client.query("select id, first_name,last_name,basic_salary,total_salary,position from employees where first_name = '#{@sc_database}'")
         if @search_result.count > 0
               @search_result.each do |v|
                   puts"\nid = #{v["id"]}\nfirst name = #{v["first_name"]}\nlast name = #{v["last_name"]} \nbasic_salary= #{v["basic_salary"]}\ntotal_salary = #{v["total_salary"]}\nposition = #{v["position"]}"
                   puts "-----------------------------------------------------------------------"
               end
                  else
           puts "record not found!!!!"
        end
       

        # if @sc_database == @search_result
        #   @search_result.each do |value|
        #   results = value["first_name"]
        #   end
          
        #  else
        #     puts "Record not found!"
        # end

end

end#end of class
