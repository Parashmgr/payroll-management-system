require "./tax_deduction"
class Newemployee
  attr_accessor :employee_first_name, :employee_last_name, :employee_num, :employee_address,:employee_email_address, :salary, :employee_salary, :employee_position

  def initialize
    @client = Database.new
  end

  def add
    puts "enter the new employee details"
    puts "employee first name"
    @employee_first_name = gets.chomp.to_s
    puts "employee last name"
    @employee_last_name = gets.chomp.to_s
    puts "empoyee address"
    @employee_address = gets.chomp.to_s
    puts "employee contact num"
    @employee_num = gets.chomp.to_i
    puts "employee email address"
    @employee_email_address = gets.chomp
    puts "employee salary"
    @salary = gets.chomp.to_i
    @employee_salary = Tax.tax_deduction(@salary)
    puts "employee position"
    @employee_position = gets.chomp.to_s
  end
end
