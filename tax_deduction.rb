require "./new_employee"
class Tax
  def self.tax_deduction(employee_salary)
    if employee_salary < 10000
      tax = 0.01
      tax_salary = employee_salary * tax 
      total_salary = employee_salary - tax_salary

    elsif employee_salary.between?(10000,25000)
      tax = 0.1
      tax_salary = employee_salary * tax      
      total_salary = employee_salary - tax_salary

    elsif employee_salary.between?(25001,50000)
      tax =  0.2
      tax_salary = employee_salary * tax
      total_salary = employee_salary - tax_salary 

    elsif employee_salary.between?(25001,100000)
      tax = 0.5
      tax_salary = employee_salary * tax
      total_salary = employee_salary - tax_salary
      
    elsif employee_salary>100001
      tax = 1
      tax_salary = employee_salary * tax
      total_salary = employee_salary - tax_salary

    end 
    return total_salary
    binding.pry

  end

end